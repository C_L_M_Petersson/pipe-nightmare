module Settings
(
    Settings,

    initialSettings,

    waterSpeed,
    tilesRequired,
    tileDims,
    hardProbability,
    tapNumber,


    fps,
    imageDir,

    tileW,
    sidePanelW,
    tilesPreviewed,


    sizes,
    waterSpeeds,
    tilesRequireds,
    hardProbabilities,
    tapNumbers,


    setWaterSpeed,
    setSize,
    setTilesRequired,
    setHardProbabilities,
    setTapNumber
) where
    import Graphics.UI.WX

    data Settings     = Settings
                      { waterSpeed      :: Double
                      , tilesRequired   :: Int
                      , tileDims        :: (Int,Int)
                      , hardProbability :: Double
                      , tapNumber       :: Int
                      }


    initialSettings   = Settings
                      { waterSpeed      = 1.0
                      , tilesRequired   = 15
                      , tileDims        = (20,15)
                      , hardProbability = 0.1
                      , tapNumber       = 1
                      }



    fps               = 60                  :: Int
    imageDir          = "resources/images/" :: String

    tileW             = 32                  :: Int
    sidePanelW        = 3*tileW             :: Int
    tilesPreviewed    = 5                   :: Int



    waterSpeeds       = [0.5
                        ,0.75
                        ,1.0
                        ,1.25
                        ,1.5
                        ,2.0
                        ,2.5
                        ,5.0
                        ,7.5]               :: [Double]

    sizes             = [(10,10)
                        ,(20,10)
                        ,(25,20)
                        ,(40,30)]           :: [(Int,Int)]

    tilesRequireds    = [0
                        ,5
                        ,10
                        ,15
                        ,20
                        ,30
                        ,40]                :: [Int]

    hardProbabilities = [0.1
                        ,0.2
                        ,0.3
                        ,0.4
                        ,0.5
                        ,0.6
                        ,0.7
                        ,0.8
                        ,0.9
                        ,1.0]               :: [Double]

    tapNumbers        = [1
                        ,2
                        ,3
                        ,4
                        ,5]                 :: [Int]



    setWaterSpeed :: Var Settings -> Double -> IO()
    setWaterSpeed vS ws = varGet vS >>= \s -> varSet vS Settings
        { waterSpeed      = ws
        , tilesRequired   = tilesRequired   s
        , tileDims        = tileDims        s
        , hardProbability = hardProbability s
        , tapNumber       = tapNumber       s
        }

    setSize :: Var Settings -> (Int,Int) -> IO()
    setSize vS xy = varGet vS >>= \s -> varSet vS Settings
        { waterSpeed      = waterSpeed      s
        , tilesRequired   = tilesRequired   s
        , tileDims        = xy
        , hardProbability = hardProbability s
        , tapNumber       = tapNumber       s
        }

    setTilesRequired :: Var Settings -> Int -> IO()
    setTilesRequired vS tR = varGet vS >>= \s -> varSet vS Settings
        { waterSpeed      = waterSpeed      s
        , tilesRequired   = tR
        , tileDims        = tileDims        s
        , hardProbability = hardProbability s
        , tapNumber       = tapNumber       s
        }

    setHardProbabilities :: Var Settings -> Double -> IO()
    setHardProbabilities vS hP = varGet vS >>= \s -> varSet vS Settings
        { waterSpeed      = waterSpeed      s
        , tilesRequired   = tilesRequired   s
        , tileDims        = tileDims        s
        , hardProbability = hP
        , tapNumber       = tapNumber       s
        }

    setTapNumber :: Var Settings -> Int -> IO()
    setTapNumber vS tN = varGet vS >>= \s -> varSet vS Settings
        { waterSpeed      = waterSpeed      s
        , tilesRequired   = tilesRequired   s
        , tileDims        = tileDims        s
        , hardProbability = hardProbability s
        , tapNumber       = tN
        }
