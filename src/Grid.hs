module Grid
(
    distributeTaps,

    drawTilesOuter,
    advanceAllWater,

    toggleTile,
) where
    import Graphics.UI.WX

    import Data.List

    import System.Random

    import GameState
    import Settings
    import Pipes
    import Water



    distributeTaps :: Settings -> Var GameState -> StdGen -> Var [[Pipe]] ->
                      IO StdGen
    distributeTaps s vGS g vPipes = foldl (>>) (return())
        (map (\(ij,f) -> addTap s vPipes $ locate ij
                                         $ getPipe 1.0 (-1-f,1.0)) ijfs)
        >> return g'
        where
            (ijfs,g') = genTaps g (tapNumber s)
                                  (tileDims s)
                                  (getLocationList $ tileDims s)

    addTap :: Settings -> Var [[Pipe]] -> Pipe -> IO()
    addTap s vPipes p = varGet vPipes >>= \ps ->
                        varSet vPipes $ fst $ updateTileGridOuter s ps p

    genTaps :: StdGen -> Int -> (Int,Int) -> [((Int,Int),Int)] ->
               ([((Int,Int),Int)],StdGen)
    genTaps g 0 _   ijs = ([],g)
    genTaps g n ijN ijs = (ijf:ijfs,g'')
        where
            (ijf ,g' ) = genTap g ijs ijN
            (ijfs,g'') = genTaps g' (n-1) ijN (filterList ijf ijs)

    genTap :: StdGen -> [((Int,Int),Int)] -> (Int,Int) ->
              (((Int,Int),Int),StdGen)
    genTap g ijs ijN = (ijf,g')
        where
            ijf        = ijs     !! ijI
            (ijI, g' ) = randomR (0,length ijs-1)     g

    getLocationList :: (Int,Int) -> [((Int,Int),Int)]
    getLocationList (iN,jN) =
        concatMap (\ ij -> map (\ f -> (ij, f)) [0, 1, 2, 3])
        $ concat [[(i,j) | i <- [1..iN-2]] | j <- [1..jN-2]]

    filterList :: ((Int,Int),Int) -> [((Int,Int),Int)] -> [((Int,Int),Int)]
    filterList ijf' []   = []
    filterList ijf' (ijf:ijfs)
        |    ij  == ij'
         ||  ij  == nij'
         || nij  ==  ij' = filterList ijf' ijfs
        | otherwise      = ijf : filterList ijf' ijfs
        where
            ij   = fst ijf
            ij'  = fst ijf'
            nij  = nextTile (mod (snd ijf +2) 4) $ fst ijf
            nij' = nextTile (mod (snd ijf'+2) 4) $ fst ijf'

--delete (nextTile (mod f 4) ij) $ delete ij ijs



    drawTilesOuter :: DC a -> Rect -> [[Pipe]] -> IO()
    drawTilesOuter dc view  = foldr ((>>) . drawTilesInner dc view) (return())

    drawTilesInner :: DC a -> Rect -> [Pipe] -> IO()
    drawTilesInner dc view = foldr (\t ->
            (>>) (drawSinglePipe t (getTileLocation $ location t) dc)
        ) (return())



    advanceAllWater :: Var GameState -> Var [[Pipe]] -> IO()
    advanceAllWater vGS vPipes = varGet vPipes          >>= \ps  ->
            wetTilesOuter vGS (map (map raiseWater) ps) >>= \ps' ->
            checkVictory  vGS (checkDiffOuter ps ps') >>
            checkEdgeOuter vGS True ps' >> varSet vPipes ps'



    checkDiffOuter :: [[Pipe]] -> [[Pipe]] -> Bool
    checkDiffOuter []     []       = False
    checkDiffOuter (p:ps) (p':ps') = checkDiffInner p p'||checkDiffOuter ps ps'

    checkDiffInner :: [Pipe] -> [Pipe] -> Bool
    checkDiffInner []     []       = False
    checkDiffInner (p:ps) (p':ps') = water p/=water p'||checkDiffInner ps ps'



    wetTilesOuter :: Var GameState -> [[Pipe]] -> IO [[Pipe]]
    wetTilesOuter vGS ps
        | length ps < 2 = return ps
        | otherwise     = wetTilesInner vGS (take 2 ps)             >>= \psI ->
                          wetTilesOuter vGS (tail psI ++ drop 2 ps) >>= \psO ->
                          return (head psI : psO)

    wetTilesInner :: Var GameState -> [[Pipe]] -> IO [[Pipe]]
    wetTilesInner vGS [p0:p0s,p1:p1s]
        | null p0s  = spreadH vGS [[p0],[p1]]
        | otherwise = wetTilesInner vGS [p0s,p1s] >>= \[p0s',p1s'] ->
                      mapM (spreadV vGS) [p0:p0s',p1:p1s']
                      >>= spreadH vGS

    spreadV :: Var GameState -> [Pipe] -> IO [Pipe]
    spreadV vGS (p0:p1:ps) = p0' >>= \p0'' -> p1' >>= \p1'' ->
                             return $ p0'':p1'':ps
        where
            p0' = if spreadsTo p1 1 then wetTile vGS p0 3 else return p0
            p1' = if spreadsTo p0 3 then wetTile vGS p1 1 else return p1

    spreadH :: Var GameState -> [[Pipe]] -> IO [[Pipe]]
    spreadH vGS [p0:p0s,p1:p1s] = p0' >>= \p0'' -> p1' >>= \p1'' ->
                                  return [p0'':p0s,p1'':p1s]
        where
            p0' = if spreadsTo p1 2 then wetTile vGS p0 0 else return p0
            p1' = if spreadsTo p0 0 then wetTile vGS p1 2 else return p1



    checkEdgeOuter :: Var GameState -> Bool -> [[Pipe]] -> IO()
    checkEdgeOuter vGS _     [p]    = checkEdgeInner vGS [3,2] p
    checkEdgeOuter vGS True  (p:ps) = checkEdgeOuter vGS False ps >>
                                      checkEdgeInner vGS [3,0] p
    checkEdgeOuter vGS False (p:ps) = checkEdgeOuter vGS False ps >>
                                      checkEdgeInner vGS [3]   p

    checkEdgeInner :: Var GameState -> [Int] -> [Pipe] -> IO()
    checkEdgeInner vGS fs (p:ps)
        | null ps     = mapM_ testLeak fs
        | fs == [3]   = foldr ((>>) . testLeak) (return()) fs >>
                        checkEdgeInner vGS [1] ps
        | 3 `elem` fs = mapM_ testLeak fs >>
                        checkEdgeInner vGS (delete 3 fs) ps
        | fs == [1]   = checkEdgeInner vGS [1] ps
        | otherwise   = foldr ((>>) . testLeak) (return()) fs >>
                        checkEdgeInner vGS fs ps
        where
            testLeak f = when (spreadsTo p (mod (f+2) 4))
                              (leak vGS (nextTile f $ location p) f)



    toggleTile :: Settings -> Var GameState -> Var [[Pipe]] -> Var [Pipe]
        -> Point -> IO()
    toggleTile s vGS vPipes vFuturePipes pt = ifRunning vGS $
        varGet vPipes       >>= \p  ->
        varGet vFuturePipes >>= \fp ->
        let ij       = getTileIndex pt
            newPipes = updateTileGridOuter s p (locate ij $ head fp)
        in varSet vPipes (fst newPipes) >>
        when (snd newPipes) (varSet vFuturePipes (tail fp))

    updateTileGridOuter :: Settings -> [[Pipe]] -> Pipe -> ([[Pipe]],Bool)
    updateTileGridOuter _ [[]]  _        = ([[]],False)
    updateTileGridOuter s pipes nextPipe
        | i' < 0 || j' < 0
                 || i' > x_max
                 || j' > y_max
                    = (pipes,False)
        | i == i'   =
            let new = updateTileGridInner (head pipes) nextPipe
            in (fst new : tail pipes, snd new)
        | otherwise =
            let new = updateTileGridOuter s (tail pipes) nextPipe
            in (head pipes : fst new, snd new)
            where
                (i',j') = location nextPipe
                (i ,_ ) = location $ head $ head pipes
                x_max = fst $ tileDims s
                y_max = snd $ tileDims s

    updateTileGridInner :: [Pipe] -> Pipe -> ([Pipe],Bool)
    updateTileGridInner []     _  = ([],False)
    updateTileGridInner (p:ps) p'
        | j == j'   = if hard p || wet (water p)
                          then (p :ps,False)
                          else (p':ps, True)
        | otherwise = let new = updateTileGridInner ps p'
                      in (p : fst new, snd new)
            where
                (_ ,j ) = location p
                (i',j') = location p'



    getTileLocation :: (Int, Int) -> Point
    getTileLocation xy = point x y
        where
            x=tileW*fst xy + sidePanelW
            y=tileW*snd xy

    getTileIndex :: Point -> (Int, Int)
    getTileIndex pt = (i, j)
        where
            i = div (pointX pt-sidePanelW) tileW
            j = div (pointY pt)            tileW

    nextTile :: Int -> (Int,Int) -> (Int,Int)
    nextTile 0 (i,j) = (i-1,j  )
    nextTile 1 (i,j) = (i  ,j+1)
    nextTile 2 (i,j) = (i+1,j  )
    nextTile 3 (i,j) = (i  ,j-1)
