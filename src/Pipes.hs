module Pipes
(
    Pipe,
    Water,

    pipeEmpty,

    hard,
    paths,
    location,
    water,
    wet,

    drawSinglePipe,

    getFuturePipes,
    getPipe,

    locate,

    wetTile,
    spreadsTo,
    raiseWater,
) where
    import Graphics.UI.WX
    import Graphics.UI.WXCore

    import Data.List

    import System.Random
    import System.IO

    import Control.Monad (join)

    import GameState
    import Settings
    import Water


    data Pipe = Pipe {
        tileBM   :: Bitmap(),
        hard     :: Bool,
        paths    :: [[Int]],
        location :: (Int,Int),
        water    :: Water
        }



    tapConstructor :: (String,String,Int,[[Int]]) -> Pipe
    tapConstructor (bm,k,r,p) = Pipe {
        tileBM   = loadPipeBM bm False,
        hard     = True,
        paths    = p,
        location = (0,0),
        water    = waterConstructor (k,r,True,True,0,[r])
        }

    pipeConstructor :: (String,String,Int,Bool,[[Int]]) -> Pipe
    pipeConstructor (bm,k,r,h,p) = Pipe {
        tileBM   = loadPipeBM bm h,
        hard     = h,
        paths    = p,
        location = (0,0),
        water    = waterConstructor (k,r,False,False,0,[])
        }

    emptyConstructor :: (String,String,Int,Bool,[[Int]]) -> (Int,Int) -> Pipe
    emptyConstructor (bm,k,r,h,p) ij = Pipe {
        tileBM   = loadPipeBM bm h,
        hard     = h,
        paths    = p,
        location = ij,
        water    = waterConstructor (k,r,False,False,0,[])
        }



    pipesStandard = map pipeConstructor [
        ("02","02",0,False,[[2],[],[0],[]]),
        ("13","02",1,False,[[],[3],[],[1]]),

        ("01","01",0,False,[[1],[0],[],[]]),
        ("12","01",1,False,[[],[2],[1],[]]),
        ("23","01",2,False,[[],[],[3],[2]]),
        ("03","01",3,False,[[3],[],[],[0]]),

        ("012","012",0,False,[[1,2],[0,2],[0,1],[]]),
        ("123","012",1,False,[[],[2,3],[1,3],[1,2]]),
        ("023","012",2,False,[[2,3],[],[0,3],[0,2]]),
        ("013","012",3,False,[[1,3],[0,3],[],[0,1]]),

        ("0123","0123",0,False,[[1,2,3],[0,2,3],[0,1,3],[0,1,2]])
        ]
    pipesHard = map pipeConstructor [
        ("02","02",0,True,[[2],[],[0],[]]),
        ("13","02",1,True,[[],[3],[],[1]]),

        ("01","01",0,True,[[1],[0],[],[]]),
        ("12","01",1,True,[[],[2],[1],[]]),
        ("23","01",2,True,[[],[],[3],[2]]),
        ("03","01",3,True,[[3],[],[],[0]]),

        ("012","012",0,True,[[1,2],[0,2],[0,1],[]]),
        ("123","012",1,True,[[],[2,3],[1,3],[1,2]]),
        ("023","012",2,True,[[2,3],[],[0,3],[0,2]]),
        ("013","012",3,True,[[1,3],[0,3],[],[0,1]]),

        ("0123","0123",0,True,[[1,2,3],[0,2,3],[0,1,3],[0,1,2]])
        ]
    pipesTap  = map tapConstructor [
        ("0_tap","0_tap",0,[[0],[],[],[]]),
        ("1_tap","0_tap",1,[[],[1],[],[]]),
        ("2_tap","0_tap",2,[[],[],[2],[]]),
        ("3_tap","0_tap",3,[[],[],[],[3]])
        ]
    pipeBG    = pipeConstructor  ("BG",   "BG",   0,False,[[],[],[],[]])
    pipeEmpty = emptyConstructor ("empty","empty",0,False,[[],[],[],[]])



    loadPipeBM :: String -> Bool -> Bitmap()
    loadPipeBM bm h = bitmap (imageDir++"tiles/"++bm++(if h then "_hard"
                                                            else "")++".png")

    drawSinglePipe :: Pipe -> Point -> DC a -> IO()
    drawSinglePipe p pt dc = drawBitmap dc (tileBM p) pt True [] >>
        when (wet $ water p) (drawWater (water p) pt dc)



    getFuturePipes :: StdGen -> Double -> [Pipe]
    getFuturePipes g pr = map (getPipe pr) $ tiles `zip` hard
        where
            tiles = randomRs (0, length pipesStandard-1) g1
            hard  = randomRs (0.0,1.0) g2
            (g1,g2) = split g

    getPipe :: Double -> (Int,Double) -> Pipe
    getPipe pr (p,t)
        | p < 0     = pipesTap      !! (-1-p)
        | t > pr    = pipesStandard !! p
        | otherwise = pipesHard     !! p



    locate :: (Int,Int) -> Pipe -> Pipe
    locate ij p = Pipe {
            tileBM   = tileBM   p,
            hard     = hard     p,
            paths    = paths    p,
            location = ij,
            water    = nextWaterLevel $ water p
        }

    raiseWater :: Pipe -> Pipe
    raiseWater p = Pipe {
            tileBM   = tileBM   p,
            hard     = hard     p,
            paths    = paths    p,
            location = location p,
            water    = nextWaterLevel $ water p
        }

    wetTile :: Var GameState -> Pipe -> Int -> IO Pipe
    wetTile vGS p f
        | null $ paths p !! f = leak vGS (location p) f >> return p
        | wet  $ water p      =                            return p'
        | otherwise           = decreaseTilesLeft vGS   >> return p'
        where p' = Pipe {
            tileBM   = tileBM   p,
            hard     = hard     p,
            paths    = paths    p,
            location = location p,
            water    = waterConstructor (bmkey $ water p,
                                         rotation $ water p,
                                         True,
                                         False,
                                         level $ water p,
                                         sort $ nub $ f : from (water p))
        }

    spreadsTo :: Pipe -> Int -> Bool
    spreadsTo p f = level (water p) == 8 &&
                    any (elem f) [paths p !! i | i <- from $ water p]
