import Graphics.UI.WX

import Data.Time

import System.Random

import GameState
import Grid
import Pipes
import Settings



main :: IO()
main = start $ pipeNightmare =<< varCreate initialSettings

newGame :: Frame() -> Var Settings -> IO()
newGame f vNS = close f >> start (pipeNightmare vNS)



pipeNightmare :: Var Settings -> IO()
pipeNightmare vCS =
    newStdGen
        >>= \g                ->

    varGet vCS
        >>= \currSet          ->

    variable      [value        := currSet]
        >>= \vNS              ->

    frameFixed    [text         := "Pipe Nightmare",
                   layout       := space
                                   (sidePanelW+tileW*fst (tileDims currSet))
                                   (     tileW+tileW*snd (tileDims currSet))]
        >>= \f                ->

    panel f       [layout       := space
                                   (sidePanelW+tileW*fst (tileDims currSet))
                                   (           tileW*snd (tileDims currSet))]
        >>= \p                ->

    varCreate     [[pipeEmpty (j,i) | i <- [0..snd (tileDims currSet)-1]]
                                    | j <- [0..fst (tileDims currSet)-1]]
        >>= \vPipes           ->

    varCreate     Prelude { tilesLeft = tilesRequired currSet }
        >>= \vGS              ->

    getCurrentTime >>= varCreate
        >>= \vClock           ->

    distributeTaps currSet vGS g vPipes
        >>= \g'               ->

    variable      [value        := getFuturePipes g' (hardProbability currSet)]
        >>= \vFuturePipes     ->

    staticText p   [ text       := "Tiles left: "
                                ++ show (tilesRequired currSet)
                   , fontWeight := WeightBold
                   , position   := point 5 (tileW*(snd (tileDims currSet)-1))
                   ]
        >>= \stTilesLeft      ->

    timer f       [ interval    := div 1000 fps
                  , on command  := timeStep vCS vGS p vPipes stTilesLeft
                                            vClock
                  ]
        >>= \t                ->

    menuPane       [ text       := "Game" ]
        >>= \pGame            ->

    menuItem pGame [ text       := "New"
                   , on command := newGame f vNS
                   ] >>
    menuItem pGame [ text       := "Pause"
                   , on command := pauseToggle vGS
                   , checkable  := True
                   ] >>
    menuItem pGame [ text       := "Exit"
                   , on command := close f
                   ] >>

    menuPane       [ text       := "Water Speed" ]
        >>= \pWaterSpeed       ->
    waterSpeedList pWaterSpeed vNS waterSpeeds >>

    menuPane       [ text       := "Size" ]
        >>= \pSize            ->
    sizeList pSize vNS sizes >>

    menuPane       [ text       := "Tiles Required" ]
        >>= \pTilesRequired   ->
    tilesRequiredList pTilesRequired vNS tilesRequireds >>

    menuPane       [ text       := "Hardness Probability" ]
        >>= \pHardProbability ->
    hardProbabilityList pHardProbability vNS hardProbabilities >>

    menuPane       [ text       := "Number of Taps" ]
        >>= \pTapNumber ->
    tapNumberList pTapNumber vNS tapNumbers >>

    button p       [ text       := "Finish"
                   , on command := ifRunning vGS $ setWaterSpeed vCS 24
                   , position   := point 5 (tileW*(snd (tileDims currSet)-3))
                   ] >>

    set f [ bgcolor := grey
          , menuBar := [ pGame
                       , pSize
                       , pWaterSpeed
                       , pTilesRequired
                       , pHardProbability
                       , pTapNumber]
          ] >>

    set p [ bgcolor  := grey
          , on paint := draw    currSet vGS vPipes vFuturePipes
          , on click := onClick currSet vGS vPipes vFuturePipes
          ]

    where
        timeStep :: Var Settings -> Var GameState -> Panel() -> Var [[Pipe]] ->
                    StaticText() -> Var UTCTime -> IO()
        timeStep vS vGS p vPipes stTilesLeft vClock =
            varGet vS  >>= \s ->
            varGet vGS >>= \gs ->
            advanceClock s vClock >>= \advance ->
            if advance then ifRunning vGS $ advanceAllWater vGS vPipes >>
                                            updateTexts vGS stTilesLeft
                       else return() >>
            repaint p

        advanceClock :: Settings -> Var UTCTime -> IO Bool
        advanceClock s vClock =
            varGet vClock  >>= \c  ->
            getCurrentTime >>= \c' ->
            if realToFrac (diffUTCTime c' c) < 1.0/waterSpeed s
                then return False
                else varSet vClock c' >> return True

        updateTexts :: Var GameState -> StaticText() -> IO()
        updateTexts vGS stTilesLeft = varGet vGS >>= \gs ->
            set stTilesLeft [ text := "Tiles left: "++show (tilesLeft gs) ]



        onClick :: Settings -> Var GameState -> Var [[Pipe]] -> Var [Pipe] ->
                   Point -> IO()
        onClick s vGS vP vFP pt = startGame vGS >>
                                  toggleTile s vGS vP vFP pt



        draw :: Settings -> Var GameState -> Var [[Pipe]] -> Var [Pipe] -> DC a
                -> Rect -> IO()
        draw s vGS vTiles vFutureTiles dc view =
            varGet vTiles       >>= drawTilesOuter dc view >>
            varGet vFutureTiles >>= drawWholeTileQueue tilesPreviewed dc view
            >> showState s vGS dc

        drawWholeTileQueue :: Int -> DC a -> Rect -> [Pipe] -> IO()
        drawWholeTileQueue  (-1) _  _    _          = return()
        drawWholeTileQueue  i    dc view (fp:fps) =
            drawSinglePipe fp pt dc >>
            drawWholeTileQueue (i-1) dc view fps
            where
                pt = point 30 (tileW*i)



        waterSpeedList :: Menu() -> Var Settings -> [Double] -> IO()
        waterSpeedList _     _   []       = return()
        waterSpeedList pWaterSpeed vNS (ws:wss) = menuItem pWaterSpeed
            [ text       := show ws
            , on command := setWaterSpeed vNS ws
            ] >> waterSpeedList pWaterSpeed vNS wss

        sizeList :: Menu() -> Var Settings -> [(Int,Int)] -> IO()
        sizeList _     _   []       = return()
        sizeList pSize vNS (yx:yxs) =
            menuItem pSize
                [ text       := show (snd yx)++"x"++show (fst yx)
                , on command := setSize vNS yx
                ] >> sizeList pSize vNS yxs

        tilesRequiredList :: Menu() -> Var Settings -> [Int] -> IO()
        tilesRequiredList _              _   []       = return()
        tilesRequiredList pTilesRequired vNS (tR:tRs) =
            menuItem pTilesRequired
                [ text       := show tR
                , on command := setTilesRequired vNS tR
                ] >> tilesRequiredList pTilesRequired vNS tRs

        hardProbabilityList :: Menu() -> Var Settings -> [Double] -> IO()
        hardProbabilityList _     _   []       = return()
        hardProbabilityList pHardProbability vNS (hP:hPs) =
            menuItem pHardProbability
                [ text       := show hP
                , on command := setHardProbabilities vNS hP
                ] >> hardProbabilityList pHardProbability vNS hPs

        tapNumberList :: Menu() -> Var Settings -> [Int] -> IO()
        tapNumberList _     _   []       = return()
        tapNumberList pTapNumber vNS (tN:tNs) =
            menuItem pTapNumber
                [ text       := show tN
                , on command := setTapNumber vNS tN
                ] >> tapNumberList pTapNumber vNS tNs
